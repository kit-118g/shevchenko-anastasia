﻿
namespace lab_1
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Birthday { get; set; }
        public string Date { get; set; }
        public string Index { get; set; }
        public string Special { get; set; }
        public string Progres { get; set; }

        public override string ToString() 
        {

            return $"Имя: {Name}\n Фамилия: {Surname}\n Отчество: {Lastname}\n День рождения: {Birthday}\n Дата поступления: {Date}\n Индекс группы: {Index}\n Успеваемость: {Progres}\n \n";

        }
        public Student() { }
        public Student(string Name, string Surname, string Lastname, string Birthday, string Date, string Index, string Special, string Progres)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Lastname = Lastname;
            this.Birthday = Birthday;
            this.Date = Date;
            this.Index = Index;
            this.Special = Special;
            this.Progres = Progres;
        }
    }
}
