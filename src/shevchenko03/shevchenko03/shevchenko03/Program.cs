﻿using System;
using System.IO;


namespace shevchenko03
{
    class Program
    {
        static void Main(string[] args)
        {
            var records = new Container();
            bool loop = true;
            string name, lastname, surname, faculty, specialty;
            string path = @"D:\C# LAB .NET\shevchenko\src\shevchenko03\shevchenko03\records.txt";
            DateTime birthday, date;
            int progress, number;
            char index;
            string text;
            int choice;
            while (loop)
            {
                Methods.Menu();
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Methods.AddStud(records);
                        break;
                    case 2:
                        foreach (var student in records)
                        {
                            Console.WriteLine(student + " ");
                        }
                        break;
                    case 3:
                        Methods.WriteFile(records, path);
                        break;
                    case 4:
                        Methods.ReadFile(records, path);
                        break;
                    case 5:
                        Console.WriteLine("Номер студента, которого хотите найти: ");
                        number = int.Parse(Console.ReadLine());
                        records.Search(records, number);
                        break;
                    case 6:
                        Console.WriteLine("Номер студента, данные о котором хотите удалить: ");
                        number = int.Parse(Console.ReadLine());
                        records.Remove(records, number);
                        break;
                    case 7:
                        Console.WriteLine("Номер студента, данные о котором хотите отредактировать: ");
                        number = int.Parse(Console.ReadLine());
                        Console.WriteLine("Что хотите отредактировать? (1-имя, 2 - фамилию, 3 - отчество, 4 - день рождения, 5 - дата поступления, " +
                                        "6 - индекс группы, 7 - факультет, 8 - специальность, 9 - успеваемость");
                        int n;
                        string str;
                        n = int.Parse(Console.ReadLine());
                        Console.WriteLine("Введите новые данные в соответствующем формате : ");
                        str = Console.ReadLine();
                        records.Edit(records, number, n, str);
                        break;
                    case 8:
                        loop = false;
                        break;
                }
            }
        }
    }
}
