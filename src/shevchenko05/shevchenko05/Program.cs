﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace shevchenko05
{
    class Program
    {
        delegate int Average(Container records, int number, String str);
        static void Main(string[] args)
        {
            string path = @"D:\C# LAB .NET\shevchenko\src\shevchenko05\records.txt";
            StringBuilder sb = new StringBuilder();
            var records = new Container();
            bool loop = true;
            int number;
            int choice;
            Average average;

            while (loop)
            {
                Methods.Menu();
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Methods.AddStud(records);
                        break;
                    case 2:
                        Console.WriteLine("Имя\t Фамилия\t Отчество\t Дата рождения\t\t Дата поступления     Индекс   Ф-тет  Спец-сть  Усп-сть");
                        Console.WriteLine("------------------------------------------------------------------------------------------------------------------------");
                        foreach (var student in records)
                        {
                            Console.WriteLine(student + " ");
                        }
                        break;
                    case 3:
                        Methods.WriteFile(records, path);
                        break;
                    case 4:
                        Methods.ReadFile(records, path);
                        break;
                    case 5:
                        Console.WriteLine("Номер студента, которого хотите найти: ");
                        number = int.Parse(Console.ReadLine());
                        records.Search(records, number);
                        break;
                    case 6:
                        Console.WriteLine("Номер студента, данные о котором хотите удалить: ");
                        number = int.Parse(Console.ReadLine());
                        records.Remove(records, number);
                        break;
                    case 7:
                        Console.WriteLine("Номер студента, данные о котором хотите отредактировать: ");
                        number = int.Parse(Console.ReadLine());
                        Console.WriteLine("Что хотите отредактировать? (1-имя, 2 - фамилию, 3 - отчество, 4 - день рождения, 5 - дата поступления, " +
                                        "6 - индекс группы, 7 - факультет, 8 - специальность, 9 - успеваемость");
                        int n;
                        string str;
                        n = int.Parse(Console.ReadLine());
                        Console.WriteLine("Введите новые данные в соответствующем формате : ");
                        str = Console.ReadLine();
                        records.Edit(records, number, n, str);
                        break;
                    case 8:
                        Console.WriteLine("Номер студента, чью группу хотите узнать: ");
                        number = int.Parse(Console.ReadLine());
                        Methods.Group(records, number);
                        break;
                    case 9:
                        Console.WriteLine("Номер студента, чей номер курса и семестра на текущий момент хотите узнать: ");
                        number = int.Parse(Console.ReadLine());
                        Methods.Course(records, number);
                        break;
                    case 10:
                        Console.WriteLine("Номер студента, чей текущий возраст хотите узнать: ");
                        number = int.Parse(Console.ReadLine());
                        Methods.Years(number, records);
                        break;
                    case 11:
                        Console.WriteLine("По какому критерию вывести список студентов? (1 - группа, 2 - специальность, 3 - факультет)");
                        int num;
                        string str1;
                        num = int.Parse(Console.ReadLine());
                        Methods.DialogMenu(num);
                        str1 = Console.ReadLine();
                        records.Print(records, num, str1);
                        break;
                    case 12:
                        Console.WriteLine("По какому критерию удалить студентов? (1 - группа, 2 - специальность, 3 - факультет)");
                        int numb;
                        string str2;
                        numb = int.Parse(Console.ReadLine());
                        Methods.DialogMenu(numb);
                        str2 = Console.ReadLine();
                        records.GrRemove(records, numb, str2);
                        break;
                    case 13:
                        Console.WriteLine("По какому критерию средний возраст студентов? (1 - группа, 2 - специальность, 3 - факультет)");
                        int num2;
                        string str3;
                        num2 = int.Parse(Console.ReadLine());
                        Methods.DialogMenu(num2);
                        str3 = Console.ReadLine();
                        average = records.AvAge;
                        Console.WriteLine("Средний возраст: " + average(records, num2, str3));
                        break;
                    case 14:
                        Console.WriteLine("По какому критерию расчитать среднюю успеваемость студентов? (1 - группа, 2 - специальность, 3 - факультет)");
                        int num3;
                        string str4;
                        num3 = int.Parse(Console.ReadLine());
                        Methods.DialogMenu(num3);
                        str4 = Console.ReadLine();
                        average = records.AvProgress;
                        Console.WriteLine("Средняя успеваемость: " + average(records, num3, str4));
                        break;
                    case 15:
                        loop = false;
                        break;
                }
            }
            XmlSerializer formatter = new XmlSerializer(typeof(Student[]));

            using (FileStream fs = new FileStream("students.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, records.students);
            }

            using (FileStream fs = new FileStream("students.xml", FileMode.OpenOrCreate))
            {
                Student[] newStud = (Student[])formatter.Deserialize(fs);

                foreach (Student p in newStud)
                {
                    Console.WriteLine(p);
                }
            }
        }
    }
}
